import React from 'react'

let Reporter = ({children, name}) => {
    return (
        <div>
            {name}: {children}
        </div>
    )
}

export default Reporter
